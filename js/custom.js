WOW.prototype.addBox = function(element) {
  this.boxes.push(element);
};

// Init WOW.js and get instance
var wow = new WOW();
wow.init();

// Attach scrollSpy to .wow elements for detect view exit events,
// then reset elements and add again for animation
$('.wow').on('scrollSpy:exit', function() {
  $(this).css({
    'visibility': 'hidden',
    'animation-name': 'none'
  }).removeClass('animated');
  wow.addBox(this);
}).scrollSpy();

$('.home-background').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.aro-pastilla').addClass('transitiom-aro-pastilla');
});

$('.home-background').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.aro-pastilla').removeClass('transitiom-aro-pastilla');
});

$('.home-background').scrollSpy();


$('.seccion-02-back').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.aro-pastilla-header').addClass('transitiom-aro-pastilla-header');
});

$('.seccion-02-back').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.aro-pastilla-header').removeClass('transitiom-aro-pastilla-header');
});

$('.seccion-02-back').scrollSpy();

$('.back-seccion-circles').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.aro-pastilla-int').addClass('transitiom-aro-pastilla-int');
});

$('.back-seccion-circles').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.aro-pastilla-int').removeClass('transitiom-aro-pastilla-int');
});

$('.back-seccion-circles').scrollSpy();

$('.melhorar-info').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.aro-pastilla-int').addClass('transitiom-aro-pastilla-int');
});

$('.melhorar-info').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.aro-pastilla-int').removeClass('transitiom-aro-pastilla-int');
});

$('.melhorar-info').scrollSpy();


$('.seccion-02-back').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.aro-int-header').addClass('transitiom-aro-int-header');
});

$('.seccion-02-back').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.aro-int-header').removeClass('transitiom-aro-int-header');
});

$('.seccion-02-back').scrollSpy();

$('.secabajocust').on('scrollSpy:enter', function() {//when scrolling and element showing in window
    $('.aro-2').addClass('custom-aro');
});

$('.secabajocust').on('scrollSpy:exit', function() {//when scrolling and element exit at window
    $('.aro-2').removeClass('custom-aro');
});

$('.secabajocust').scrollSpy();


$(window).scroll(function() {    

    var scroll = $(window).scrollTop();

    if (scroll >= 500) {
        $(".footer-flechas").addClass("opacity-1");
    }else {
      $(".footer-flechas").removeClass("opacity-1");
    }
});